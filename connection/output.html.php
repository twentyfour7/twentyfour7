<!/*
 * Connect to the database
 * CSE 110 Project - TwentyFour7
 * Author: Cindy Yu
 * Date: January 25, 2015
 *
 */>

<! Generic template that simply outputs the value of the output variable>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>PHP Output</title>
    <meta http-equiv="content-type"
      content="text/html; charset=utf-8"/>
  </head>
  <body>
    <p>
      <?php echo $output; ?>
    </p>
  </body>
</html>
