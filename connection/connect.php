<?php
$servername = 'localhost';
$username = 'root';
$password = 'TwentyFour7';
$dbname = 'TwentyFour7';

// Store the connect that we establish with the mysql database
$link = mysqli_connect($servername,$username,$password);

// If we are unable to connect to the database (web), then exit
// and provide the output.html.php webpage.
if( !$link){
  $output = 'Unable to connect to the database server.';
  include 'output.html.php';
  exit();
}

// Assuming the connection succeeds, set the text format:
mysqli_set_charset($link, 'utf8');

// Checks if the text format was set to utf8
if(!mysqli_set_charset($link, 'utf8'))
{
  $output = 'Unable to set database connection encoding.';
  include 'output.html.php';
  exit();
}

// Select the database you want to use
mysqli_select_db($link, $dbname);

if(!mysqli_select_db($link, $dbname))
{
  $output = 'Unable to locate the $dbname database.';
  include 'output.html.php';
  exit();
}

$output = 'Database connection established.';
include 'output.html.php';
?>
