/*
 * CSE 110 Project - TwentyFour7
 * Author: Cindy Yu
 * Date: January 25, 2015
 *
 */

<?php
$servername = 'localhost';
$username = 'root';
$password = 'TwentyFour7';
$dbname = 'TwentyFour7';

// Store the connect that we establish with the mysql database
$link = mysqli_connect($servername,$username,$password);

// If we are unable to connect to the database (web), then exit 
// and provide the output.html.php webpage.
if( !$link){
  $output = 'Unable to connect to the database server.';
  include 'output.html.php';
  exit();
}

// Assuming the connection succeeds, set the text format:
mysqli_set_charset($link, 'utf8');

// Checks if the text format was set to utf8
if(!mysqli_set_charset($link, 'utf8'))
{
  $output = 'Unable to set database connection encoding.';
  include 'output.html.php';
  exit();
}

// Select the database you want to use
mysqli_select_db($link, $dbname);

if(!mysqli_select_db($link, $dbname))
{
  $output = 'Unable to locate the $dbname database.';
  include 'output.html.php';
  exit();
}

$output = 'Database connection established.';
include 'output.html.php';
?>

/////////////////////////////////////////////////////////////////////////
////////////////////////////SCRIPT///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

// Uses an associative array to store and pull the username & password
// from the html webpage.
$name = $_POST['Username'];

$password = $_POST['Password'];

// If the username contains "admin" in it, then display the admin webpages
if(strpos($name, 'admin') !== false){
  echo 'Welcome, oh glorious leader!';
}
// Else, display the regular retail customer greeting pages
else{
echo 'Welcome to our website, ' . 
     htmlspecialchars($name, ENT_QUOTES, 'UTF-8'). ' ' . 
     htmlspecialchars($password, ENT_QUOTES, 'UTF-8') . '!';
}
?>
