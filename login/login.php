<?php
// Uses an associative array to store and pull the username & password
// from the html webpage.
$name = $_POST['Username'];

$password = $_POST['Password'];

// If the username contains "admin" in it, then display the admin webpages
if(strpos($name, 'admin') !== false){
  echo 'Welcome, oh glorious leader!';
}
// Else, display the regular retail customer greeting pages
else{
echo 'Welcome to our website, ' . 
     htmlspecialchars($name, ENT_QUOTES, 'UTF-8'). ' ' . 
     htmlspecialchars($password, ENT_QUOTES, 'UTF-8') . '!';
}

?>
