
<?php
 //require_once("./include/membersite_config.php"); 

 session_start();
 if(!isset($_SESSION['username'])){
 	header("Location:../login.php");
 } 
 
 $db = mysql_connect("localhost","root","TwentyFour7");
 if (!$db) {
 die("Database connection failed miserably: " . mysql_error());
 }
 $db_select = mysql_select_db("TwentyFour7",$db);
 if (!$db_select) {
 die("Database selection also failed miserably: " . mysql_error());
 }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
		table, th, td {
			border: 1px solid #333;
		}
	</style>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				  
                <a class="navbar-brand" href="#">Start Bootstrap</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
		    <li>
			<a href="admin-addservices.html.php">Customer Account</a>
		    </li>
 		    <li>
			<a href="onlinebill.html.php">Bill</a>
		    </li>
		    <li>
		 	<a href="mktg-addtopackages.html.php">Add Onto Packages</a>
		    </li>
		    <li>	
			<a href="mktg-addservice.html.php">Add Service</a>
		    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
            <h1>A Warm Welcome!</h1>
            <p>The team 24/7 runs on sugar...and sugar..and more sugar. </p>
			 <p>Please pick the services that you would like to add to your account.</p>
            <p><a class="btn btn-primary btn-large">Call to action!</a>
            </p>
        </header>

        <hr>

        <!-- Title -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Latest Features</h3>
	<form action="insert.php" method="post">
		<p>
		<label for="p1">Package</label>
		<input type="text" name="p1" id="p1">
		</p>
		<input type="submit" value="Add">
		<input type="submit" value="Remove">
	</form>
<?php 
if(!empty($_POST)){
 $value = $_POST['p1'];
 echo $value;
}
?>
            <div class="container">
<h2> Available Services: </h2>
<?php
 $result = mysql_query("SELECT * FROM products_1", $db);
 if (!$result) {
 die("Database query failed: " . mysql_error());
 }
 while ($row = mysql_fetch_array($result)) {
 echo "<p>";
 echo "Service ".$row[1].": ".$row[2]."<br />";
 echo "</p>";
 }
?>            </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        <div class="row text-center">

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                        <h3>Feature Label</h3>
                        <p>Premium Package</p>
                        <p>
                            <a input type="text" name="list[]" class="btn btn-primary">Buy Now!</a> 
                            <a href="#" class="btn btn-default">More Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                        <h3>Feature Label</h3>
                        <p>Package A</p>
                        <p>
                            <a href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                        <h3>Feature Label</h3>
                        <p>Package B</p>
                        <p>
                            <a href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                        <h3>Feature Label</h3>
                        <p>Package C</p>
                        <p>
                            <a href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
		    <?php
 			echo "Logged in as: " . $_SESSION['username'] . ".";
		    ?>
                    <p>Copyright &copy; 24/7 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
   
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script>
      function toggleText() {
        var x = "Added";
        document.getElementById("demo").innerHTML = x;
      }
      function removeText() {
        var x = " ";
        document.getElementById("demo").innerHTML = x;
      }
    </script>
	
</body>

</html>
<?php
 mysql_close($db);
?>
