<?php
if(!isset($_POST['firstname']))
{
  include 'form.html.php';
}

else
{
  // Uses an associative array to store and pull the username & password
  // from the html webpage.
  $name = $_POST['Username'];

  $password = $_POST['Password'];

  // If the username contains "admin" in it, then display the admin webpages
  if(strpos($name, 'admin') !== false){
    $output = 'Welcome, oh glorious leader!'; 
  }
  // Else, display the regular retail customer greeting pages
  else{
  $output = 'Welcome to our website, ' .
       htmlspecialchars($name, ENT_QUOTES, 'UTF-8'). ' ' .
       htmlspecialchars($password, ENT_QUOTES, 'UTF-8') . '!';
  }
  
  include 'welcome.html.php';
 
}
?>
