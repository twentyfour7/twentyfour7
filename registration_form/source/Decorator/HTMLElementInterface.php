 <?php

 interface HTMLElementInterface
 {
    public function getText();
    public function render();

 }

 class Span implements HTMLElementInterface
 {
 	protected $text;
	var $tablename = 'Menu';

 	public function __construct($text){
 		if(!is_string($text) || empty($text))
 		{
 			throw new InvalidArgumentException(
 				"The text of the element must be a non-empty string.");
 		}
 		$this->text = $text;
 	}

 	public function getText()
 	{
 		return $this->text;
 	}

	public function render()
	{
		return "<span>" . $this->text . "</span>";
	}

	function Ensuretable()
    	{
        	$result = mysql_query("SHOW COLUMNS FROM $this->tablename");   
       		if(!$result || mysql_num_rows($result) <= 0)
        	{
           	 return $this->CreateTable();
       	 	}
        return true;
    	}
    
    	function CreateTable()
    	{
        	$qry = "Create Table $this->tablename (".
                	"id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                	"name VARCHAR( 128 ) NOT NULL ,".
                	"linkpath VARCHAR( 64 ) NOT NULL ".
                	")";
        	if(!mysql_query($qry,$this->connection))
        	{
           	 $this->HandleDBError("Error creating the table \nquery was\n $qry");
            	return false;
        	}
        	return true;
    }
}

abstract class AbstractHTMLDecorator implements HTMLElementInterface
{
	protected $element;

	public function __construct(HTMLElementInterface $element)
	{
		$this->element = $element;
	}

	public function getText()
	{
		return $this->element->getText();
	}

	public function render()
	{
		return $this->element->render();
	}
}

class DivDecorator extends AbstractHTMLDecorator
{
	public function render($string){
		return "<div class=\"row text-center\">" . $this->element->render() . "</div>";
	}
}

class ParagraphDecorator extends AbstractHTMLDecorator
{
	public function render()
	{
		return "<p>" . $this->element->render() . "</p>";
	}
}

class SectionDecorator extends AbstractHTMLDecorator
{
	public function render()
	{
		return "<section>" . $this->element->render() . "</section>";
	}
}

class NavigationDecorator extends AbstractHTMLDecorator
{
	public function render()
	{
		return "<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">".
    "</nav>"';
	}
}

class ListDecorator extends AbstractHTMLDecorator
{
	protected $link = "admin-addservices.html.php";
	public function render()
	{
		return "<a href="'.$link.'">Link</a>";
		//return "<a href = " . $link . ">"  . $this->element->render() . "</a>";
	}
}

class FeaturePackage extends AbstractHTMLDecorator
{
  public function render($num)
  {
echo "HERE";
    $package = "<div class=\"col-md-3 col-sm-6 hero-feature\">\n";
    $package .= "<div class=\"thumbnail\">\n";
    $result = mysql_query("SELECT image FROM product_packages WHERE package_id=\"".$num."\" LIMIT 1");
    $img = mysql_fetch_array($result);
    $package .= "<img src=\"".$img."\" alt=\"\">\n";
    $package .= "<div class=\"caption\">\n";
    $pkg_name = mysql_query("SELECT name FROM packages WHERE id=\"".$num."\"");
    $caption = mysql_fetch_array($pkg_name);
    $package .= "<h3>".$caption."</h3>\n";
    $package .= "</div>\n";
    $package .= "</div>\n";
    $package .= "</div>\n";
  }
}
