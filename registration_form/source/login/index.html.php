
<?php
 //require_once("./include/membersite_config.php"); 

 session_start();
 if(!isset($_SESSION['username'])){
 	header("Location:../login.php");
 } 
 // Connect to database
 include('connectdb.php');
 include('../Decorator/Menu.php');
 include('Services.php');
 include('../Decorator/FeaturePackage.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
		table, th, td {
			border: 1px solid #333;
		}
	</style>
</head>

<body>

<?php
try {
  $build = new Menu;
  echo $build->displayMenu();
} catch (Exception $e) {
  echo $e->getMessage().PHP_EOL.$e->getTraceAsString();
}
?>
    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
            <h1>A Warm Welcome!</h1>
            <p>The team 24/7 runs on sugar...and sugar..and more sugar. </p>
			 <p>Please pick the services that you would like to add to your account.</p>
            </p>
        </header>

        <hr>

        <!-- Title -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Latest Features</h3>
	<form action="insert.php" method="post">
		<p>
		<label for="p1">Package</label>
		<input type="text" name="p1" id="p1">
		</p>
		<input type="submit" value="Add">
		<input type="submit" value="Remove">
	</form>
<?php 
if(!empty($_POST)){
 $value = $_POST['p1'];
 echo $value;
}
?>
            <div class="container">
<h2> Available Services: </h2>
<?php
try {
  $build = new Services;
  echo $build->prices();
} catch (Exception $e) {
  echo $e->getMessage().PHP_EOL.$e->getTraceAsString();
}
?>
     </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        <div class="row text-center">

<?php
try {
  $pkg = new FeaturePackage;
  echo $pkg->render(1);
} catch (Exception $e) {
  echo $e->getMessage().PHP_EOL.$e->getTraceAsString();
}
?>

<?php
try {
  $pkg = new FeaturePackage;
  echo $pkg->render(2);
} catch (Exception $e) {
  echo $e->getMessage().PHP_EOL.$e->getTraceAsString();
}
?>

<?php
try {
  $pkg = new FeaturePackage;
  echo $pkg->render(3);
} catch (Exception $e) {
  echo $e->getMessage().PHP_EOL.$e->getTraceAsString();
}
?>

<?php
try {
  $pkg = new FeaturePackage;
  echo $pkg->render(4);
} catch (Exception $e) {
  echo $e->getMessage().PHP_EOL.$e->getTraceAsString();
}
?>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
		    <?php
 			echo "Logged in as: " . $_SESSION['username'] . ".";
		    ?>
                    <p>Copyright &copy; 24/7 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
   
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script>
      function toggleText() {
        var x = "Added";
        document.getElementById("demo").innerHTML = x;
      }
      function removeText() {
        var x = " ";
        document.getElementById("demo").innerHTML = x;
      }
    </script>
	
</body>

</html>
<?php
 mysql_close($db);
?>
